# dokyll

A simple [Docker][] image with [Jekyll][] inside. Listens on port 4000.

Build the bundle cache:

```shell
docker run --rm \
   -v "$PWD:/mnt" \
   -v "$PWD/_bundle:/usr/local/bundle" \
   bundle install
```


Build the site, continuously:

```shell
MULTICONFIG=''
# MULTICONFIG='--config _config.yml,_local_config.yml'
docker run --rm \
   -v "$PWD:/mnt" \
   -v "$PWD/_bundle:/usr/local/bundle" \
   bundle exec jekyll build $MULTICONFIG --watch
```

Serve the site (only):

```shell
MULTICONFIG=''
# MULTICONFIG='--config _config.yml,_local_config.yml'
docker run --rm \
   -p 4000:4000 \
   -v "$PWD:/mnt" \
   -v "$PWD/_bundle:/usr/local/bundle" \
   bundle exec jekyll serve $MULTICONFIG \
   --no-watch --skip-initial-build --host=0.0.0.0
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.


The contents of `pack/create.sh` are *heavily* inspired to
[https://github.com/envygeeks/jekyll-docker](https://github.com/envygeeks/jekyll-docker),
although not the same because there's no variable here.

[Docker]: https://www.docker.com/
[Jekyll]: https://jekyllrb.com/
