#!/bin/sh
exec 1>&2  # send everything to log

srcdir="$(cat DIBS_DIR_SRC)"
cp "$srcdir"/suexec "$srcdir"/dibs-jekyll.txt /

BUNDLE_HOME=/usr/local/bundle
BUNDLE_APP_CONFIG=/usr/local/bundle
BUNDLE_BIN=/usr/local/bundle/bin
GEM_BIN=/usr/gem/bin
GEM_HOME=/usr/gem
JEKYLL_VAR_DIR=/var/jekyll
JEKYLL_DATA_DIR=/srv/jekyll
JEKYLL_BIN=/usr/jekyll/bin
JEKYLL_ENV=development

addgroup -Sg 1000 jekyll
adduser  -Su 1000 -G jekyll jekyll

apk update
apk add --no-cache \
  zlib-dev \
  build-base \
  libxml2-dev \
  libxslt-dev \
  readline-dev \
  libffi-dev \
  yaml-dev \
  zlib-dev \
  libffi-dev \
  cmake \
  linux-headers \
  openjdk8-jre \
  less \
  zlib \
  libxml2 \
  readline \
  libxslt \
  libffi \
  git \
  nodejs \
  tzdata \
  shadow \
  bash \
  su-exec \
  nodejs-npm \
  libressl \
  yarn

echo "gem: --no-ri --no-rdoc" > ~/.gemrc
unset GEM_HOME && unset GEM_BIN && yes | gem update --system
unset GEM_HOME && unset GEM_BIN && yes | gem install --force bundler
gem install jekyll -v4.0.0 -- --use-system-libraries
gem install \
   html-proofer \
   jekyll-reload \
   jekyll-mentions \
   jekyll-coffeescript \
   jekyll-sass-converter \
   jekyll-commonmark \
   jekyll-paginate \
   jekyll-compose \
   jekyll-assets \
   RedCloth \
   kramdown \
   jemoji \
   jekyll-redirect-from \
   jekyll-sitemap \
   jekyll-feed minima \
   -- --use-system-libraries

mkdir -p $JEKYLL_VAR_DIR
mkdir -p $JEKYLL_DATA_DIR
chown -R jekyll:jekyll $JEKYLL_DATA_DIR
chown -R jekyll:jekyll $JEKYLL_VAR_DIR
chown -R jekyll:jekyll $BUNDLE_HOME

rm -rf /root/.gem
rm -rf /home/jekyll/.gem
rm -rf $BUNDLE_HOME/cache
rm -rf $GEM_HOME/cache
